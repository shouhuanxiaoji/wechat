# OpenCloudOS 安装 Linux 微信  

#### 介绍
Linux 微信支持移动端和 PC 端文件互相传输，支持语音和视频，支持朋友圈等功能，在 OpenCloudOS 安装 Linux 微信，支持 X86、ARM 架构。  
  
#### 安装教程
  
1. 下载 [OpenCloudOS Stream ISO 镜像](https://www.opencloudos.org/ospages/downloadISO)，选择 X86/ARM 架构   
![输入图片说明](image/1.choose_ISO_x86orarm.jpg)   
  
2. 选择桌面登录：GNOME Xorg      
![输入图片说明](image/2.choose_GNOME_Xorg.jpg)  
  
3. 安装依赖包和微信 RPM 包（以 x86 为例）       
  
- 安装依赖包指令：  
> rpm -ivh libldevice-0.2-1.ocs23.x86_64.rpm --force     
![输入图片说明](image/3.install_libldevice.jpg)    
  
- 安装微信 RPM 包指令： 
> rpm -ivh wechat-beta_1.0.0.242_amd64.rpm      
![输入图片说明](image/4.install_wechat.jpg)    
  
4. 登录微信   
![输入图片说明](image/5.login_wechat.jpg)  
  
#### 问题反馈 
欢迎加入 OpenCloudOS 社区群交流（社区大使微信：qingmin0623）    